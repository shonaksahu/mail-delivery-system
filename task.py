# email_system/tasks.py

from celery import shared_task
from django.core.mail import send_mail
from maildeliverysystem.models import Event
from django.utils import timezone

@shared_task
def send_event_emails():
    current_date = timezone.now().date()
    events = Event.objects.filter(event_date=current_date, sent=False)
    
    for event in events:
      
        email_content = f"Happy {event.event_type} to {event.employee.name}!"
        try:
            send_mail(
                subject=f"Event Notification: {event.event_type}",
                message=email_content,
                from_email="shonak.cgs@gmail.com",
                recipient_list=[event.employee.email],
            )
        
            event.sent = True
            event.save()
        except Exception as e:
   
            pass
