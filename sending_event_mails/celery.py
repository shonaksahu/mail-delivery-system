# celery.py

from __future__ import absolute_import, unicode_literals
import os
from sending_event_mails.sending_event_mails.celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sending_event_mails.settings')

app = Celery('sending_event_mails')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
