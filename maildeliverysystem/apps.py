from django.apps import AppConfig


class MaildeliverysystemConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'maildeliverysystem'
