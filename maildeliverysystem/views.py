from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Event
from .serializers import EventSerializer
from django.utils import timezone
from django.core.mail import send_mail
from rest_framework import viewsets
from .models import Employee, Event
from .serializers import EmployeeSerializer, EventSerializer

class EmployeeViewSet(viewsets.ModelViewSet):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer

class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer

class SendEventEmails(APIView):
    def get(self, request):
        current_date = timezone.now().date()
        events = Event.objects.filter(event_date=current_date, sent=True)
        
        for event in events:
            # Fetch email template and populate it with event-specific content
            email_content = f"Happy {event.event_type} to {event.employee.name}!"
            try:
                send_mail(
                    subject=f"Event Notification: {event.event_type}",
                    message=email_content,
                    from_email="shonak.cgs@gmail.com",
                    recipient_list=[event.employee.email],
                )
                # Mark the event as sent
                event.sent = True
                event.save()
            except Exception as e:
                pass
        
        return Response({"message": "Event emails sent successfully."}, status=status.HTTP_200_OK)
