# email_system/urls.py

from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import EmployeeViewSet, EventViewSet

router = DefaultRouter()
router.register(r'employees', EmployeeViewSet)
router.register(r'events', EventViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
